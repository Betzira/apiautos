import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutosComponent } from './autos.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      component: AutosComponent,
  },
// Dentro del autos.module vas a poner eso antes del @NgModule
];

@NgModule({
  declarations: [AutosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class AutosModule { }






