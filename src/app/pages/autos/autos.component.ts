import { Component, OnInit } from '@angular/core';
import { AutosService } from '../../service/autos.service';

@Component({
  selector: 'app-autos',
  templateUrl: './autos.component.html',
  styleUrls: ['./autos.component.css']
})
export class AutosComponent implements OnInit {

  new_var:any;

  constructor( private api : AutosService ) { }

  ngOnInit(): void {
    // console.log("funciona")
    this.api.DatosByPrincipal().subscribe(variable => {
      this.new_var = variable;
      console.log('resultado', this.new_var); //,
    });
  }

}
