import { Component, OnInit } from '@angular/core';
import { AutosService } from '../../service/autos.service';

@Component({
  selector: 'app-valor',
  templateUrl: './valor.component.html',
  styleUrls: ['./valor.component.css']
})
export class ValorComponent implements OnInit {

  // data:any;
  data = [];

  constructor( private api : AutosService ) { }

  ngOnInit(): void {

    // console.log("funciona")
    // this.api.DatosByValor().subscribe(variable => {
    //   this.precio = variable;
    //   console.log('resultado', this.precio); //,
    // });

    this.api.DatosByValor().subscribe(
      (resp: any) => {
        console.log(resp);
        this.data = resp.data;
      }
    )

  }
}
