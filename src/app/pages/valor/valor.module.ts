import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ValorComponent } from './valor.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      component: ValorComponent,
  },
// Dentro del autos.module vas a poner eso antes del @NgModule
];

@NgModule({
  declarations: [ValorComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class ValorModule { }
