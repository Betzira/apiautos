import { Component, OnInit } from '@angular/core';
import { AutosService } from '../../service/autos.service';

@Component({
  selector: 'app-modelos',
  templateUrl: './modelos.component.html',
  styleUrls: ['./modelos.component.css']
})
export class ModelosComponent implements OnInit {

  new_v:any;

  constructor( private api : AutosService ) { }

  ngOnInit(): void {

    // console.log("funciona")
    this.api.DatosByModelos().subscribe(variable => {
      this.new_v = variable;
      console.log('resultado', this.new_v); //,
    });

  }

  // onClick(modelo){
  //   console.log(modelo)
  // }
 
}
