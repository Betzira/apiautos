import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModelosComponent } from './modelos.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      component: ModelosComponent,
  },
// Dentro del autos.module vas a poner eso antes del @NgModule
];

@NgModule({
  declarations: [ModelosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class ModelosModule { }
