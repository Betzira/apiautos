import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MotosComponent } from './motos.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
      path: '',
      component: MotosComponent,
  },
// Dentro del autos.module vas a poner eso antes del @NgModule
];

@NgModule({
  declarations: [MotosComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class MotosModule { }
