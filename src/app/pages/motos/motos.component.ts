import { Component, OnInit } from '@angular/core';
import { AutosService } from '../../service/autos.service';

@Component({
  selector: 'app-motos',
  templateUrl: './motos.component.html',
  styleUrls: ['./motos.component.css']
})
export class MotosComponent implements OnInit {

  motos:any;

  constructor( private api : AutosService) { }

  ngOnInit(): void {

    // console.log("funciona")
    this.api.DatosByMotos().subscribe(variable => {
      this.motos = variable;
      console.log('resultado', this.motos); //,
    });

  }

}
