import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AutosComponent } from './pages/autos/autos.component'

const routes: Routes = [
  {
    path: 'autos',
    loadChildren: () => import('./pages/autos/autos.module').then( module => module.AutosModule)
  },
  {
    path: 'modelos',
    loadChildren: () => import('./pages/modelos/modelos.module').then( module => module.ModelosModule)
  },
  {
    path: 'valor',
    loadChildren: () => import('./pages/valor/valor.module').then( module => module.ValorModule)
  },
  {
    path: 'motos',
    loadChildren: () => import('./pages/motos/motos.module').then( module => module.MotosModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
