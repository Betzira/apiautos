import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AutosService {

  url: string = "https://parallelum.com.br/fipe/api/v1/carros/marcas";

  url2: string = "https://parallelum.com.br/fipe/api/v1/carros/marcas/59/modelos/5940/anos";

  url3: string = "https://parallelum.com.br/fipe/api/v1/carros/marcas/59/modelos/5940/anos/2020-3";

  url4: string = "https://parallelum.com.br/fipe/api/v1/motos/marcas";

  constructor(private http: HttpClient) { 

   }

   DatosByPrincipal(){
      const url = this.url;
      // return this.http.get(url);
      return this.http.get(`https://parallelum.com.br/fipe/api/v1/carros/marcas`);
    }
  
    DatosByModelos(){
      const url2 = this.url2;
      // return this.http.get(url2);
      return this.http.get(`https://parallelum.com.br/fipe/api/v1/carros/marcas/59/modelos/5940/anos`);
    }

    DatosByValor(){
      const url3 = this.url3;
      // return this.http.get(url2);
      return this.http.get(`https://parallelum.com.br/fipe/api/v1/carros/marcas/59/modelos/5940/anos/2020-3`);
      //https:parallelum.com.br/fipe/api/v1/motos/marcas
    }
    
    DatosByMotos(){
      const url4 = this.url4;
      // return this.http.get(url);
      return this.http.get(`https://parallelum.com.br/fipe/api/v1/motos/marcas`);
    }

  }
